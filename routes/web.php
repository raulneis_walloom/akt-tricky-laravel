<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// CORS
header('Access-Control-Allow-Origin: *');

Route::get('/', 'GamesController@index');

//Alerts
Route::get('/success/{id}', 'GamesController@success');
Route::get('/won', 'GamesController@won');
Route::get('/youlost', 'GamesController@youlost');
Route::get('/finalizar', 'GamesController@finalizar');

Route::post('/save_player',array('uses'=>'PlayerController@store'));

Route::get('/games/diagrama/{game_id}',array('uses'=>'GamesController@diagrama'));



Route::get('/games/tricky', 'GamesController@tricky');
Route::get('/games/tricky/won', 'GamesController@trickyWon');
Route::get('/games/tricky/lost', 'GamesController@trickyLost');
Route::post('/games/tricky/results', 'GamesController@trickyResults');
Route::get('/api/questions', 'ApiQuestionsController@index');
Route::resource('/admin/questions', 'QuestionsController');
