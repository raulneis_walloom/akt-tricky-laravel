<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    public $table = 'players';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'last_name',
        'email',
        'attemps_number',
        'last_score',
        'level',
        'status',
        'won'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    protected $casts = [
        'name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'attemps_number' => 'number',
        'last_score' => 'number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];


    public function games() {
        return $this->hasMany('App\Game');
    }
}
