<?php

namespace App\Http\Controllers;

use App\Question;
use App\Http\Requests\CreateQuestionsRequest;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();
        return view('questions.index', ['questions' => $questions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateQuestionsRequest $request)
    {
        $image = $request->file('image')->store('image');
        $imageName = preg_replace('#^.+\/#', '', $image);

        rename(storage_path('app/'.$image), public_path('images/question-images/'.$imageName));

        $question = Question::create([
            'text' => $request->input('text'),
            'image' => $imageName,
            'r1' => $request->input('r1'),
            'r2' => $request->input('r2'),
            'r3' => $request->input('r3'),
        ]);
   
        return redirect('/admin/questions')->with('status', 'La pregunta se creó correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        return view('questions.edit', ['question' => $question]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $imageName = $question->image;

        if ($request->file('image')) {
            $image = $request->file('image')->store('image');
            $imageName = preg_replace('#^.+\/#', '', $image);

            rename(storage_path('app/'.$image), public_path('images/question-images/'.$imageName));
        }

        $question->update([
            'text' => $request->input('text'),
            'r1' => $request->input('r1'),
            'r2' => $request->input('r2'),
            'r3' => $request->input('r3'),
            'image' => $imageName,
        ]);

        return redirect('/admin/questions')->with('status', 'La pregunta se actualizó correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        $question->delete();

        return redirect('/admin/questions')->with('status', 'La pregunta fue eliminada correctamente');
    }
}
