<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class ApiQuestionsController extends Controller
{
    public function index()
    {
        $questions = Question::all();

        $questions = $questions->shuffle()->map(function (Question $question) {
            $answers = [[
                'answer' => $question->r1,
                'isCorrect' => true,
            ], [
                'answer' => $question->r2,
                'isCorrect' => false,
            ], [
                'answer' => $question->r3,
                'isCorrect' => false,
            ]];

            shuffle($answers);

            return [
                'id' => $question->id,
                'question' => $question->text,
                'image' => asset('images/question-images/'.$question->image),
                'answers' => $answers,
            ];
        });


        return ['questions' => $questions];
    }
}
