<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Player;
use App\TrickyGame;
use DB;

class GamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $player = array();
        if(@$_GET['id']){
            $player_id = $_GET['id'];
            $player = Player::where('id', $player_id)->first(); 

            if (!$player) {
                $player = Player::create([
                    'id' => $_GET['id'],
                    'name' => '-',
                    'last_name' => '-',
                    'email' => $_GET['id'].'@noemail.com',
                ]);
            }
        }
        
        $game_id = 2;
        $data = $this->getData();
        $options = $this->getOption($data,$game_id);
        return view('games.index', compact('data','options','game_id','player'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Player  $player
     * @return \Illuminate\Http\Response
     */
    public function diagrama($game_id)
    {
        $diagram_id = $this->_parserCurrentDiagram($game_id);
        $data = $this->getData();
        $options = $this->getOption($data,$diagram_id);
        return view('games.diagrama', compact('data','options','diagram_id','game_id'));
    }


    public function _parserCurrentDiagram($id)
    {
        $_get = [1=>0,2=>1,3=>2];
        return $_get[$id];
    }

    public function getData()
    {
        $path = storage_path() . "/app/data.json";
        $data = json_decode(file_get_contents($path), true);
        return $data;
    }


    public function getOption($data,$id)
    {
        $options = $data['questions']['diagramas'][$id]['options'];
        $options = array_values(array_sort($options, function ($value) {
            return $value['key'];
        }));
        return $options;
    }

    //Alerts
    public function success($id)
    {
        return view('games.success', compact('id'));
    }

    public function youlost()
    {
        return view('games.youlost');
    }

    public function won()
    {
        return view('games.won');
    }





    /**
     * Display the specified resource.
     *
     * @param  \App\Player  $player
     * @return \Illuminate\Http\Response
     */
    public function tricky()
    {
        $player = array();
        if(@$_GET['id']){
            $player_id = $_GET['id'];
            $player = Player::where('id', $player_id)->first();
            if (!$player) {
                $player = Player::create([
                    'id' => $_GET['id'],
                    'name' => '-',
                    'last_name' => '-',
                    'email' => $_GET['id'].'@noemail.com',
                ]);
            }
        }

        return view('games.tricky', ['player' => $player]);
    }

    public function trickyResults(Request $request)
    {
        $player = $this->getPlayer($request);
        
        $game = new TrickyGame();
        $game->won = $request->result == 'X';
        $game->data = $request->data ?: '{}';
        $game->player_id = $player->id;
        $game->save();

        return [
            'id' => $game->id
        ];
    }



    protected function getPlayer($request)
    {
        $player = Player::where('email', $request->email)->first();
        if (!$player) {
            $player = new Player();
            $player->name = $request->name;
            $player->last_name = $request->last_name;
            $player->email = $request->email;
            $player->save();
        }
        return $player;
    }

    public function trickyLost()
    {
        return view('games.tricky-youlost');
    }

    public function trickyWon()
    {
        return view('games.tricky-won');
    }
}
