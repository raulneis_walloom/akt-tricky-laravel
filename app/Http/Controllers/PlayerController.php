<?php

namespace App\Http\Controllers;

use App\Player;
use App\Game;
use Illuminate\Http\Request;
use DB;
use Log;

class PlayerController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $player = $this->_setPlayer($request);
            if ($request['game_id'] == 1) {
                $game = $this->_setGame($request,$player);
            }
            else
            {
                $game = $this->_getGame($request,$player);
            }            

            $check_attemps = $this->_checkAttemps($game);

            if (!$check_attemps) {
                $response = ['status'=>false, 'message'=>'Superaste el limite de intento permitido!'];
                return response()->json($response);
            }

            $get_options_correct = $this->_searchCorrectOptions($request['diagram_id']);


            $status_selected = $this->_verifyOptionsSelected($request,$get_options_correct,$game);

            Log:info('status_selected', $status_selected );

            DB::commit();

        } catch (Exception $e) {

            DB::rollback();
            Log::error('store',['error'=>$e]);
        }

        return response()->json($status_selected);
    }

    public function _searchCorrectOptions($id)
    {
        return $this->_getDiagramaCorrectOptions($this->getData(),$id);
    }

    public function _getDiagramaCorrectOptions($data,$id)
    {
        return $data['questions']['diagramas'][$id]['correct_options'];
    }

    public function _verifyOptionsSelected($request,$get_options_correct, $game)
    {
        $is_diagram_correct = false; $is_correct_selected_1 = false; $is_correct_selected_2 = false; $is_correct_selected_3 = false; $is_correct_selected_4 = false;

        $response = ['ok'=>false,'selected_incorrect'=>[], 'message'=>'Selección correcta!', 'game_id'=>$request['game_id'], 'next_game_id'=>$request['game_id'] + 1];
        

        if ($request['diagram_option_value'] == $request['game_id'] ) {
            $is_diagram_correct = true;
        }

        if (!$is_diagram_correct) {
            $response['ok'] = $is_diagram_correct;
            $response['message'] ='El diagrama seleccionado es incorrecto!';
            $response['diagram_option_value'] = $request['diagram_option_value'];
            $game->attemps_number++;
            $game->save();
            $response['game'] = $game;
            return $response;
        }

        /**/
        if ( in_array($request->input_component_selected_1, $get_options_correct) ) {
            $is_correct_selected_1 = true;
        }

        if ( in_array($request->input_component_selected_2, $get_options_correct) ) {
            $is_correct_selected_2 = true;
        }
        
        if ( in_array($request->input_component_selected_3, $get_options_correct) ) {
            $is_correct_selected_3 = true;
        }
        
        if ( in_array($request->input_component_selected_4, $get_options_correct) ) {
            $is_correct_selected_4 = true;
        }

        /**/
        
        if ( !in_array($request->input_component_selected_1, $get_options_correct) ) {
            $response['selected_incorrect'][1] = $request->input_component_selected_1;
            //array_push($response['selected_incorrect'], $request->input_component_selected_1);
        }
        if ( !in_array($request->input_component_selected_2, $get_options_correct) ) {
            $response['selected_incorrect'][2] = $request->input_component_selected_2;
            //array_push($response['selected_incorrect'], $request->input_component_selected_2);
        }        
        if ( !in_array($request->input_component_selected_3, $get_options_correct) ) {
            $response['selected_incorrect'][3] = $request->input_component_selected_3;
            //array_push($response['selected_incorrect'], $request->input_component_selected_3);
        }        
        if ( !in_array($request->input_component_selected_4, $get_options_correct) ) {
            $response['selected_incorrect'][4] = $request->input_component_selected_4;
            //array_push($response['selected_incorrect'], $request->input_component_selected_4);
        }

        $response['ok'] = $is_correct_selected_1 && $is_correct_selected_2 && $is_correct_selected_3 && $is_correct_selected_4 && $is_diagram_correct;
        if (!$response['ok']) {
            $response['message'] = 'Selección incorrecta!';
            $game->attemps_number++;
        }
        if ($response['ok']) {
            $game->score++;
        }

        if ($game->score >= 3) {
            $game->won = 1;
        }

        $game->save();

        $response['game'] = $game;

        Log::info('game', [$response['game']]);
        
        return $response;
    }

    public function _getDiagrama($request)
    {        
        Log::info('_getDiagrama',[$request]);
        return $request;
    }

    public function _checkAttemps($game)
    {        
        return $game->attemps_number < 3;
    }

    public function _setPlayer($request)
    {
        $player = Player::where('email', $request->email)->first();
        if (!$player) {
            $player = new Player();
            $player->name = $request->name;
            $player->last_name = $request->last_name;
            $player->email = $request->email;
            $player->save();
        }
        return $player;
    }

    public function _setGame($request,$player)
    {
        $game = new Game();
        $game->attemps_number = 0;
        $game->score = 0;
        $game->won = 0;
        $player->games()->save($game);
        return $player->games()->orderBy('created_at', 'desc')->first();
    }

    public function _getGame($request,$player)
    {
        return $player->games()->orderBy('created_at', 'desc')->first();
    }


    public function _getQuestion($request)
    {
        $data = $this->getData();
        return $data['questions'][$request];
    }



    public function getOption($data,$index,$id)
    {
        $options = array_values(array_sort($data['questions'][$index]['diagramas'][$id]['options'], function ($value) {
            return $value['name'];
        }));
        return $options;
    }

    public function get_diagramaById($id)
    {
        $array_diagramas = [1=>0,2=>1,3=>2];
        return $array_diagramas[$id];
    }


    public function getData()
    {
        return json_decode(file_get_contents(storage_path() . "/app/data.json"), true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Player  $player
     * @return \Illuminate\Http\Response
     */
    public function show(Player $player)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Player  $player
     * @return \Illuminate\Http\Response
     */
    public function edit(Player $player)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Player  $player
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Player $player)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Player  $player
     * @return \Illuminate\Http\Response
     */
    public function destroy(Player $player)
    {
        //
    }
}
