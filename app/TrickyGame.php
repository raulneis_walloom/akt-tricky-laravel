<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrickyGame extends Model
{
    public $table = 'tricky_games';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'player_id',
        'won',
        'data'
    ];

    public function player()
    {
        return $this->belongsTo('App\Player');
    }
}
