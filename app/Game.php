<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public $table = 'games';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'player_id',
        'attemps_number',
        'score',
        'won'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    protected $casts = [
        'player_id' => 'number',
        'attemps_number' => 'number',
        'score' => 'number',
        'won' => 'boolean'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];
    public function player() {
        return $this->belongsTo('App\Player');
    }
}
