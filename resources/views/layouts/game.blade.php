<!DOCTYPE html>
<html>
  <head>
    <title>Tricky</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Font Awesome -->
    <script src="https://use.fontawesome.com/96cbd61ead.js"></script>

    <!--STYLES-->
    <link rel="stylesheet" href="{{ asset('dist/production.min.347797a1.css') }}">
    <!--STYLES END-->
    <style type="text/css">
      .modal-dialog {
        pointer-events: auto !important;
      }
    </style>
    <script> window.baseUrl = '{{ url('') }}' </script>
  </head>

  <body style="background: url('{{ asset('images/bg_2.jpeg') }}'); opacity: 1;">
    <div id="page-wrap">
      @yield('body')
    </div>

    <script src="https://checkout.stripe.com/checkout.js"></script>

    <script>delete window.self;</script>

    <!--SCRIPTS-->
    <script src="{{ asset('dist/production.min.2196b178.js') }}"></script>
    <!--SCRIPTS END-->
  </body>
</html>
