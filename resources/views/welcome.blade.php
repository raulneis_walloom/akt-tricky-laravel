<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!-- <link href="css/cover.css" rel="stylesheet"> -->
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Game</title>
    </head>
    <body class="background">
        <div class="container pt-5">
            <div class="row align-items-center align-self-center justify-content-center">
                <div class="col-lg-12">
                    <div class="jumbotron mt-3 pt-5">
                        <h4 class="ribbon mt-3 text-center right-arrow">El taller ha sufrido una catástrofe y los diagramas electrónicos de varias conexiones de algunos componentes de la moto están desordenados y no están claros.</h4>

                        <div class="container mt-1 pt-1">                            
                            <h4 class="text-justify">En esta ocasión, necesitamos que busque e identifique los planos que muestren correctamente las conexiones y los lleve hasta el tablero para completar con éxito el ejercicio.</h4>

                            <h4 class="text-justify">Debe eliminar los diagramas que no se encuentran terminados o cuyas conexiones no son correctas.</h4>
                        </div>


                        <div class="col-lg-12 mt-3 pt-2">
                            <div class="row align-items-center">
                                <div class="col-lg-2">
                                    <div class="eye-danger">
                                        <span class="fz-35 vertical-align fa fa-exclamation-triangle"></span> ¡OJO!
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                    <div class="eye-light">
                                        <span class="fz-35 vertical-align fa fa-clock-o"></span> El tiempo es limitado.
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-lg-10 offset-lg-2">
                                    <div class="eye-light">
                                        <span class="fz-35 vertical-align fa">1</span> Solo hay una oportunidad por cada plano que seleccione.
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-lg-12 text-right">
                                    <div class="mt-3">
                                        <button type="button" class="btn btn-lg btn-secondary"><i class="fa fa-hand-pointer-o"></i> Haz click para comenzar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>