@extends('layouts.app-admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Preguntas</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>
                        <a href="{{ url('/api/questions') }}" class="float-right" target="_blank">Ver JSON <i class="fa fa-external-link-alt"></i></a>
                        <a href="{{ action('QuestionsController@create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva pregunta</a>
                    </p>
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width: 40px;"></th>
                                <th>Pregunta</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($questions as $question)
                            <tr>
                                <td style="text-align: center;"><img src="{{ url('images/question-images/'.$question->image) }}" alt="" style="max-width: 60px; max-height: 60px; margin: -0.75rem;"></td>
                                <td style="vertical-align: middle;">{{ $question->text }}</td>
                                <td class="text-right">
                                    <a href="{{ action('QuestionsController@edit', $question->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i> Editar</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
