@extends('layouts.app-admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Nueva Pregunta</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form action="{{ action('QuestionsController@store') }}" enctype="multipart/form-data" method="post">
                        @method('post')
                        @csrf

                        <div class="form-group">
                            <label for="text">Texto*</label>
                            <input type="text" class="form-control" id="text" name="text" placeholder="Texto" autofocus value="{{ old('text') }}">
                        </div>
                        <div class="form-group">
                            <label for="image">Imagen*</label>
                            <input type="file" class="form-control-file" id="image" name="image" placeholder="Imagen">
                        </div>
                        <hr>
                        <h5>Respuestas</h5>
                        <small class="text-muted">Las respuetas se mostraran en orden aleatorio.</small>
                        <div class="form-group">
                            <label for="r1">Respuesta 1</label>
                            <input type="text" class="form-control" id="r1" name="r1" placeholder="Texto"  value="{{ old('r1') }}">
                            <small id="emailHelp" class="form-text text-muted">Esta es la respuesta correcta</small>
                        </div>
                        <div class="form-group">
                            <label for="r2">Respuesta 2</label>
                            <input type="text" class="form-control" id="r2" name="r2" placeholder="Texto"  value="{{ old('r2') }}">
                        </div>
                        <div class="form-group">
                            <label for="r3">Respuesta 3</label>
                            <input type="text" class="form-control" id="r3" name="r3" placeholder="Texto"  value="{{ old('r3') }}">
                        </div>

                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Guardar</button>
                        <a href="{{ action('QuestionsController@index') }}" class="btn btn-default">Volver</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
