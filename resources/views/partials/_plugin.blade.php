<script type="text/javascript">
    
    
    function selected_option(obj, value) {
        const diagram_options = document.querySelectorAll('.diagram_option');
        for (const diagram_option of diagram_options) {
            document.getElementById(diagram_option.id).classList.remove('selected');
    
            setDiagramOptionValue(value);
        }
        document.getElementById(obj.id).classList.add('selected');
    }
    
    function setDiagramOptionValue(value) {
        var x = document.getElementById("diagram_option_value");
        x.value = value;
        toggle_visibility('result', 'block');
        setResultValue(value);
    }
    
    function toggle_visibility(id, value) {
        var e = document.getElementById(id);
        e.style.display = value;
    }
    
    function setResultValue(value) {
        document.getElementById("result").innerHTML = "Diagrama de <i class='float-right text-white'> <span class='fa fa-check-square-o'></span> Opcion: " + value + "</i>";
    }
    
    
    var allowDrop_value = '';
    
    function allowDrop(ev) {
        console.log({allowDrop: ev})
        ev.preventDefault();
        allowDrop_value = ev.target.id;
    }
    
    function drag(ev) {
        console.log({drag: ev})
        ev.dataTransfer.setData("text", ev.target.id);
    }
    
    function drop(ev) {
        console.log({drop: ev})
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        ev.target.appendChild(document.getElementById(data));
        var img1 = document.getElementById(data);
        var img2 = document.getElementById(allowDrop_value);
        img2.classList.remove("bg-fill");
        img2.src = img1.src;
        var input = document.getElementById("input_" + allowDrop_value);
        var st = data.split('_');
        input.value = st[1];
    }

    function destroyLocalPlayer() {
        // localStorage.removeItem('player');
    }

    function sync_data(components) {
        console.log(components)
        var player = JSON.parse(localStorage.getItem('player'));
        var url = "{{URL::to('/')}}/save_player";
        var name = player.name;
        var last_name = player.last_name;
        var email = player.email;
        var diagram_id = "{{ $diagram_id }}";
        var game_id = "{{ $game_id }}";
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            },
            type: "POST",
            dataType: 'JSON',
            url: url,
             data: {
                name: name,
                last_name: last_name,
                email: email,
                input_component_selected_1: components.input_component_selected_1,
                input_component_selected_2: components.input_component_selected_2,
                input_component_selected_3: components.input_component_selected_3,
                input_component_selected_4: components.input_component_selected_4,
                diagram_option_value: components.diagram_option_value,
                diagram_id,diagram_id,
                game_id,game_id,
                _token: '{{csrf_token()}}'
            },
             success: function( msg ) {
                $('#confirm-selection-1').attr('disabled',true);
                //$("#ajaxResponse").append("<div class='alert alert-info mt-3'>"+msg.message+"</div>");

                console.log(msg.selected_incorrect);
                $('#confirm-selection-1').attr('disabled',true);

                jQuery.each( msg.selected_incorrect, function( i, val ) {
                    console.log(".component_select_" + i);
                  $( ".component_select_" + i ).addClass('error');
                });

                if ('diagram_option_value' in msg) {
                  $( "#option_" + msg.diagram_option_value ).removeClass('selected');
                  $( "#option_" + msg.diagram_option_value ).addClass('error');
                }
                
                
                localStorage.setItem('msg',JSON.stringify(msg));
                localStorage.setItem('game'+game_id,msg.ok);
                if(msg.status == false){
                    alert(msg.message);
                    window.location.href = "{{URL::to('/')}}";
                }
                
                if (msg.ok) {
                    $('#success').modal('show');
                } else {
                    $('#bad').modal('show');
                }

                $('.next-up').click(function () {   
                    setTimeout(function() {
                        if (msg.next_game_id < 4) {
                            if (msg.game.attemps_number >= 2 && parseInt(msg.game.score) == 0) {
                                console.log('...')
                                window.location.href = "{{URL::to('/')}}/youlost";
                                return
                            }
                            window.location.href = "{{URL::to('/')}}/games/diagrama/"+msg.next_game_id;
                        } else {
                            if (msg.game.attemps_number >= 2) {
                                window.location.href = "{{URL::to('/')}}/youlost";
                            }
                            if (msg.game.score >= 2) {
                                localStorage.removeItem('msg');
                                localStorage.removeItem('game1');
                                localStorage.removeItem('game2');
                                localStorage.removeItem('game3');
                                window.location.href = "{{URL::to('/')}}/won";
                            }
                        }
                    }, 200);
                })
            }
        });
    }

    function send_data() {
        
        var input_component_selected_1 = $('#input_component_selected_1').val();
        var input_component_selected_2 = $('#input_component_selected_2').val();
        var input_component_selected_3 = $('#input_component_selected_3').val();
        var input_component_selected_4 = $('#input_component_selected_4').val();

        if ( input_component_selected_1 == 0 && input_component_selected_1 == "0") {
            alert("Debes seleccionar los 4 componentes para continuar.");
            return false;
        }
        if ( input_component_selected_2 == 0 && input_component_selected_2 == "0") {
            alert("Debes seleccionar los 4 componentes para continuar.");
            return false;
        }
        if ( input_component_selected_3 == 0 && input_component_selected_3 == "0") {
            alert("Debes seleccionar los 4 componentes para continuar.");
            return false;
        }
        if ( input_component_selected_3 == 0 && input_component_selected_3 == "0") {
            alert("Debes seleccionar los 4 componentes para continuar.");
            return false;
        }
        if ( input_component_selected_4 == 0 && input_component_selected_4 == "0") {
            alert("Debes seleccionar los 4 componentes para continuar.");
            return false;
        }

        var diagram_option_value = $('#diagram_option_value').val();

        if (diagram_option_value == "") {
            alert("Debes indicar a cual de los 3 diagramas pertenecen.");
            return false;
        }

        let components = {
            input_component_selected_1:input_component_selected_1,
            input_component_selected_2:input_component_selected_2,
            input_component_selected_3:input_component_selected_3,
            input_component_selected_4:input_component_selected_4,
            diagram_option_value:diagram_option_value
        };
        sync_data(components);   
    }
</script>