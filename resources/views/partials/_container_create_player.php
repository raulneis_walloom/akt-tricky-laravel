<!-- Modal -->
<div class="modal modal-primary fade create_player" id="create_player" tabindex="-1" role="dialog" aria-labelledby="create_player" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="form-player">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white text-center" id="">Datos del jugador</h5>                
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" class="form-control" id="name" placeholder="Name" required>
                </div>
                <div class="form-group">
                    <label for="last_name">Apellido</label>
                    <input type="text" class="form-control" id="last_name" aria-describedby="last_name" placeholder="Last Name" required>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" aria-describedby="email" placeholder="Email" required>
                </div>
            </div>
            <div class="modal-footer">
                <button id="save_local" type="button" class="btn btn-primary"><span class="fa fa-check"></span> Confirmar</button>
            </div>
        </form>
        </div>
    </div>
</div>