
<div id="container_2" class="container pt-5">
    <div class="row">
        <div class="col-lg-12">
            <div class="jumbotron">
                <div class="row">   
                    <div class="col-lg-6">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-lg-12 text-center">
                                    <div id="clockdiv" class="text-center">
                                        <h3 id="title-remaining">
                                            Tiempo restante: <label id="time" class="time text-danger"></label>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-lg-12 text-center">
                                    @foreach($options as $img)
                                    <div class="component">
                                        <img id="component_{{ $img['key'] }}" class="fill" src="{{URL::asset('/assets/img/diagramas/')}}/{{ $img['name'] }}" draggable="true" ondragstart="drag(event)" title="">
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="container">
                            <div class="row align-items-center">
                                <h6 class="text-justify">
                                    {{ $data['questions']['description'] }}
                                </h6>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-lg-12 text-center">
                                    <?php
                                        for ($i=1; $i < 5; $i++) { if ($i == 3){ echo "<br>"; } ?>
                                        <div class="component component_select_{{$i}}">
                                            <!-- input component_selected_{{$i}} -->
                                            <input type="hidden" id="input_component_selected_{{$i}}" name="component_selected_{{$i}}" value="0">
                                            <!-- img component_selected_{{$i}} -->
                                            <img id="component_selected_{{$i}}" class="fill bg-fill"  draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)">
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-lg-12 text-center pt-3">
                                    <input type="hidden" id="diagram_option_value" name="diagram_option_value" readonly="readonly" required="">
                                    <input type="hidden" id="diagram_id" name="diagram_id" readonly="readonly" value="{{$diagram_id}}" required="">
                                    <input type="hidden" id="game_id" name="game_id" readonly="readonly" value="{{$game_id}}" required="">
                                    <div class="eye-danger text-left font-weight-bold" id="result">
                                        Diagrama de
                                    </div>
                                    @foreach($data['questions']['options'] as $option)
                                    <div id="option_{{ $option['key'] }}" title="" class="diagram_option eye-light pointer" onclick="selected_option(this, {{ $option['key'] }} );">
                                        {{$option['description']}}
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-lg-12 text-center pt-3">
                                    <button type="button" id="confirm-selection-1" onclick="send_data();" class="btn btn-outline-primary">
                                        <span class="fa fa-check"></span> Confirmar
                                    </button>
                                </div>
                                <div class="col-lg-12 text-center pt-3">
                                    <div id="ajaxResponse"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal modal-primary fade show" id="right-answer" style="background: none;" tabindex="-1" role="dialog" aria-labelledby="right-answer" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            {{-- <div class="modal-header">
                <h5 class="modal-title">Resultado:</h5>
            </div> --}}
            <div class="modal-body">
                <br>
                <div id="right-answer-right">
                    <div class="alert alert-success" role="alert">
                        Has respondido correctamente!
                    </div>
                </div>
                <div id="right-answer-wrong">
                    <div class="alert alert-danger" role="alert">
                        La respuesta no es correcta.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="next-up">Continuar <i class="fa fa-chevron-right"></i></button>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal modal-primary fade " id="bad" tabindex="-1" role="dialog" aria-labelledby="bad" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <a href="#" class="next-up">
                <div class="modal-body bad">
                	<label id="time" class="time text-dange" style="display: none;"></label>
                </div>
            </a>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal modal-primary fade" id="success" tabindex="-1" role="dialog" aria-labelledby="success" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <label id="time" class="time text-dange" style="display: none;"></label>
            <a href="#" class="next-up">
                <div class="modal-body success">
                </div>
            </a>
        </div>
    </div>
</div>

@include('partials._plugin')