<div id="container_1" class="container pt-5">
    <div class="row">
        <div class="col-lg-12">
            <div class="jumbotron">
                <label id="time" class="time text-dange" style="display: none;"></label>
                <h5 class="ribbon text-center right-arrow">
                    {{ $data['title'] }}                
                </h5>
                <div class="container">
                    <h5 class="text-justify mt-3">
                        {{ $data['subtitle1'] }}
                    </h5>
                    <h5 class="text-justify mt-3 mb-3">
                        {{ $data['subtitle2'] }}
                    </h5>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="eye-warning eye-rounded">
                                <span class="fa fa-exclamation-triangle"></span> Advertencia!
                            </div>
                        </div>
                        <div class="col-lg-10">
                            <div class="eye-light eye-rounded">
                                <span class="fa fa-clock-o"></span> El tiempo es limitado.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10 offset-lg-2">
                            <div class="eye-light eye-rounded">
                                <span class="fa">1</span> Solo hay una oportunidad por cada plano que seleccione.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 offset-lg-5">
                            <div class="mt-3 text-center">
                                <!-- <a class="text-center img-responsive">
                                    <img id="begin_game" class="text-center pointer border-round w-100" src="{{URL::asset('/assets/img/icons/btn-game.PNG')}}" title="Haz click para comenzar" alt="Haz click para comenzar">
                                </a> -->

                                <a class="text-center img-responsive" href="{{URL::to('/')}}/games/diagrama/1">
                                    <img id="" class="text-center pointer border-round w-100" src="{{URL::asset('/assets/img/icons/btn-game.PNG')}}" title="Haz click para comenzar" alt="Haz click para comenzar">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-1">
                    <!-- <a id="close_player" href="#" title="Cerrar" class="btn btn-outline-danger">
                        <i class="fa fa-window-close" aria-hidden="true"></i> Cerrar
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
