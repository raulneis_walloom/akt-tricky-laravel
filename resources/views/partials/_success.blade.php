<!-- Modal -->
<div class="modal modal-primary fade open_modal" id="success" tabindex="-1" role="dialog" aria-labelledby="success" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content ">
            <label id="time" class="time text-dange" style="display: none;"></label>
            <a title="" href="{{URL::to('/')}}/games/diagrama/{{ $id+1 }}">
                <div class="modal-body success">
                </div>
            </a>
        </div>
    </div>
</div>