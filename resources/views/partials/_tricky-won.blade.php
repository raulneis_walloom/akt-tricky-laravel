<!-- Modal -->
<div class="modal modal-primary fade open_modal" id="won" tabindex="-1" role="dialog" aria-labelledby="won" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <label id="time" class="time text-dange" style="display: none;"></label>
            <a id="close_player" href="{{ url('games/tricky') }}">
                <div class="modal-body won">
                </div>
            </a>
        </div>
    </div>
</div>