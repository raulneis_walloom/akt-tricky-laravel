<!-- Modal -->
<div class="modal modal-primary fade open_modal" id="you_lost" tabindex="-1" role="dialog" aria-labelledby="you_lost" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <a href="{{ url('games/tricky') }}" id="close_player_lost" class="pointer">
                <div class="modal-body you_lost">
                	<label id="time" class="time text-dange" style="display: none;"></label>
                </div>
            </a>
        </div>
    </div>
</div>
