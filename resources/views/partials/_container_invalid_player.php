<!-- Modal -->
<div class="modal modal-danger fade invalid_player" id="invalid_player" tabindex="-1" role="dialog" aria-labelledby="invalid_player" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="form-player">
            <div class="modal-header bg-danger">
                <h5 class="modal-title text-white text-center" id="">Detalles inválidos</h5>                
            </div>
            <div class="modal-body">
                <p>
                    Por favor agregue la identificación de usuario correcta
                </p>
            </div>
            
        </form>
        </div>
    </div>
</div>