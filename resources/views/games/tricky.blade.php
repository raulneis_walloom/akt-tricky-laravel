@extends('layouts.game')

@section('body')
<style>
    .cell {
        padding: 0;
    }
    .cell.wrong {
        border: 4px solid darkred;
        background: darkred;
    }
    .cell.right {
        border: 4px solid darkgreen;
        background: darkgreen;
    }
    .cell img {
        display: block;
        max-width: 100%;
        max-height: 100%;
    }

    #question-block {
        padding-right: 50px;
        padding-left: 0;
    }
    #question-block-inner { 
        padding: 30px 50px;
        background-color: #D0403D;
        border-bottom-right-radius: 25px;
        border-top-right-radius: 25px;
    }
    #question-block-inner * {
        opacity: 1;
    }

    #form-question {
        text-align: left;
        color: #fff;
    }
    #form-options {
        text-align: left;
    }
    .question-option .btn-secondary {
        background-color: #fff;
        color: #333;
        width: 400px;
        text-align: left;
    }
    #form-options span {
        padding-left: 10px;
    }

    #image-block {
        padding-right: 0;
        text-align: right
    }

    #form-image {
        max-height: 361px;
    }
</style>

    @if ($player)
        <script type="text/javascript">
            localStorage.setItem('player', JSON.stringify({
                name: '{{ $player->name }}',
                last_name: '{{ $player->last_name }}',
                email: '{{ $player->email }}'
            }))
            window.location.href = '{{ url('games/tricky') }}'
		</script>
    @endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-2">
                <img alt="Logo" src="{{ asset('images/logo2.png') }}"/>
                <!-- Button trigger modal - Instructions -->
                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#instructions">
                    <img alt="Instructions" src="{{ asset('images/icon1.png') }}"/>
                </button>
            </div>
            <div class="col-md-10">
                <div class="wrapper">
                    <div id="tacGame">
                        <!-- <h1 id="reset">Refresh</h1> -->
                        <p><button class="cell" data-pos="0,0" data-used="0"></button><button class="cell" data-pos="0,1" data-used=0"></button><button class="cell" data-pos="0,2" data-used="0"></button></p>
                        <p><button class="cell" data-pos="1,0" data-used="0"></button><button class="cell" data-pos="1,1" data-used=0"></button><button class="cell" data-pos="1,2" data-used="0"></button></p>
                        <p><button class="cell" data-pos="2,0" data-used="0"></button><button class="cell" data-pos="2,1" data-used=0"></button><button class="cell" data-pos="2,2" data-used="0"></button></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
     
    <script>
        var questions = []
        var answer = false;
        var body = $('#tacGame');
        var reset = $('#reset');
        var btns = $("#tacGame p button ");

        var init = function (){
            ownblocks=[];
            loseblocks=[];
            [].forEach.call(btns,function(cur) {
                cur = $(cur)
                cur.html('')
                cur.data('used', '') 
                cur.data('value', '') 
                cur.removeAttr('disabled')
                cur.removeClass('wrong')
                cur.removeClass('right')
            });

            var cells = $('.cell')

            questions = shuffle(questions)

            cells.each(function (i, cell) {
                var index = i % questions.length
                $(cell).data('question', index)
                $(cell).html('<img src="'+ questions[index].image +'">')
            })
        };

        $(window).on('load', function () {
            $.get("{{ url('/api/questions') }}", function (data) {
                questions = data.questions
                init()
            })
        });

        $(document).on('click', '#form-options label', function () {
            setTimeout(function () {
                $('#btn-seleccion').removeAttr('disabled')
            }, 1)
        })

        $( document ).ready(function(){

            let player = localStorage.getItem('player');
        
            if (player == null) {
                $('.invalid_player').modal({
                    backdrop: 'static',
                    keyboard: false
                })
                return
            }

            $('#instructions').modal('show');

            var ownblocks, loseblocks;

            // init();

            var posX = 0,
                posY = 0;
            reset.onclick = init;

            var setOnClick = function(cur, index, arr) {
                cur.onclick = function() {
                    return function() {
                        var $cur = $(cur)
                        if ($cur.data('used') !== '') {
                            return;
                        }

                        console.log($cur.data('question'))

                        var query = questions[$cur.data('question')];

                        $('#form-id').val(query.id)
                        $('#form-question').html(query.question)
                        $('#form-cell').val($(cur).data('pos'))
                        
                        $('#form-options').html('')
                        $('#btn-seleccion').attr('disabled', 'disabled')
                        let i = 1
                        for (var a in shuffle(query.answers)) {
                            var answer = query.answers[a]

                            $('#form-options').append(
                                $('<div class="question-option">').append(
                                    $('<label class="btn btn-secondary">').append(
                                        $('<input type="radio" name="options" id="option'+i+'" class="form-option" autocomplete="off" name="optradio" value="'+a+'">')
                                    ).append(
                                        $('<span id="form-option'+i+'">'+answer.answer+'</span>')
                                    )
                                )
                            )
                            // console.log('{{ asset('') }}' , query.image)

                            $('#form-image').attr('src', query.image)

                            i++
                        }

                        var modal = $("#questionModal").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    };
                }();
            };

            [].forEach.call(btns, setOnClick);
        });

        function validarTest() {
            var cel = $('button[data-pos=\''+$('#form-cell').val()+'\']')
            var value = $('.form-option:checked').val()
            var qid = $('#form-id').val()

            for (var q in questions) {
                var question = questions[q]

                if (question.id != qid) {
                    continue
                }

                if (question.answers[value].isCorrect) { 
                    cel.data('used', 'X')
                    cel.addClass('right')
                } else {
                    cel.data('used', 'O')
                    cel.addClass('wrong')
                }

                break
            }

            $('#questionModal').modal('hide')

            // console.log('checkGame', checkGame())

            var result = checkGame()
            setTimeout(function () {
                if (result !== false) {
                    sendResult(result)
                }
            }, 100)
            
        }

        function checkGame() {
            var vals = {}


            for (var x = 0; x < 3; x++) {
                for (var y = 0; y < 3; y++) {
                    var pos = x + ',' + y
                    var cel = $('button[data-pos=\''+pos+'\']')
                    vals[pos] = cel.data('used') !== '' ? cel.data('used') : false
                }
            }

            // console.log('vals', vals)
            for (var i in ['X']) {
                var s = ['X'][i]
                if (
                    (vals['0,0'] === s && vals['0,1'] === s && vals['0,2'] === s) ||
                    (vals['1,0'] === s && vals['1,1'] === s && vals['1,2'] === s) ||
                    (vals['2,0'] === s && vals['2,1'] === s && vals['2,2'] === s) ||
                    (vals['0,0'] === s && vals['1,0'] === s && vals['2,0'] === s) ||
                    (vals['0,1'] === s && vals['1,1'] === s && vals['2,1'] === s) ||
                    (vals['0,2'] === s && vals['1,2'] === s && vals['2,2'] === s) ||
                    (vals['0,0'] === s && vals['1,1'] === s && vals['2,2'] === s) ||
                    (vals['2,0'] === s && vals['1,1'] === s && vals['0,2'] === s)
                ) {
                    return s
                }
            }

            return (
                vals['0,0'] !== false && vals['0,1'] !== false && vals['0,2'] !== false &&
                vals['1,0'] !== false && vals['1,1'] !== false && vals['1,2'] !== false &&
                vals['2,0'] !== false && vals['2,1'] !== false && vals['2,2'] !== false
            ) ? 'O': false
        }

        function shuffle(a) {
            for (let i = a.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [a[i], a[j]] = [a[j], a[i]];
            }
            return a;
        }

        function sendResult(result)
        {
            var player = JSON.parse(localStorage.getItem('player'));
            $.post('{{ url('games/tricky/results') }}', {
                name: player.name,
                last_name: player.last_name,
                email: player.email,
                result: result
            }, function( data ) {
                if (result == 'X') {
                    window.location.href = '{{ url('games/tricky/won') }}'
                } else if (result == 'O') {
                    window.location.href = '{{ url('games/tricky/lost') }}'
                }
            })
        }
    </script>

    <div class="modal " id="questionModal" tabindex="-1" role="dialog" aria-labelledby="questionModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-full" role="document">
            <div class="container-fluid">
                <div class="row">
                    <div id="image-block" class="col-md-6 bg-light">
                        <img id="form-image" src="#" class="img-fluid" alt="Responsive image">
                    </div>
                    <div id="question-block" class="col-md-6">
                        <div id="question-block-inner">
                            <p id="question"></p>
                            <form action="#">
                                <input type="hidden" id="form-id" value="">
                                <input type="hidden" id="form-cell" value="">
                                <p><h2 id="form-question"></h2></p>

                                <p><div data-toggle="buttons" id="form-options"></div></p>


                                <button type="button" id="btn-seleccion" class="btn btn-primary" onclick="validarTest()" disabled>Enviar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="instructions" tabindex="-1" role="dialog" aria-labelledby="questionModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="questionModalLabel"><b>INSTRUCCIONES PARA JUGAR LINEA DE 3</b></h3>
                </div>
                <div class="modal-body">
                    <div>
                            <p>
                                <h4><span class="badge badge-danger">1</span> Da click sobre alguno
                                    de los 9 cuadros.</h4>
                            </p>
                            <p>
                                <h4><span class="badge badge-danger">2</span> Lee la pregunta.</h4>
                            </p>
                            <p>
                                <h4><span class="badge badge-danger">3</span> Contesta la pregunta, al acertar se marca el símbolo en el
                                    cuadro al que pertenece.</h4>
                            </p>
                            <p>
                                <h4><span class="badge badge-danger">4</span> Si al terminar el juego no logras la línea de 3, puedes intentar
                                    otra partida.</h4>
                            </p>
                            <br>
                            <p>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <h2>Comienza. ¡Te deseamos todos los éxitos!</h2>
                                </button>
                            </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal modal-danger fade invalid_player" id="invalid_player" tabindex="-1" role="dialog" aria-labelledby="invalid_player" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form id="form-player">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title text-white text-center" id="">Detalles inválidos</h5>                
                </div>
                <div class="modal-body">
                    <p>
                        Por favor agregue la identificación de usuario correcta
                    </p>
                </div>
                
            </form>
            </div>
        </div>
    </div>


    @include('partials._won')
    @include('partials._youlost')
@endsection