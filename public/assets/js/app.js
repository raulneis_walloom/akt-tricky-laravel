jQuery(function($) {
    "use strict";

    var minutes = 0;
    var display = document.querySelector('#time');
    minutes = 60 * 5;
    //startTimer(minutes, display);

    function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };
  
    $(document).on("keydown", disableF5);

    //check localstorage player
    if (!getLocalPlayer()) {
        return
    }

    function validateForm() {
        var name = document.forms["form-player"]["name"].value;
        if (name == "") {
            //alert("El Nombre es requerido");
            document.getElementById("name").focus();
            return false;
        }

        var last_name = document.forms["form-player"]["last_name"].value;
        if (last_name == "") {
            //alert("El Apellido es requerido");
            document.getElementById("last_name").focus();
            return false;
        }

        var email = document.forms["form-player"]["email"].value;
        if (email == "") {
            //alert("El Email es requerido");
            document.getElementById("email").focus();
            return false;
        }
        var save_local = $('#save_local').attr('disabled',true);
    }

    function check_player() {
        var name = $('#name').val();
        var last_name = $('#last_name').val();
        var email = $('#email').val();

       if (name == "" || last_name== "" || email == "") {
        open_modal_player();
       }
    }

    function open_modal_player() {
        $('.invalid_player').modal({
            backdrop: 'static',
            keyboard: false
        });
    }

    $('.open_modal').modal({
        backdrop: 'static',
        keyboard: false
    });

    $('[data-toggle="tooltip"]').tooltip();


    $("button#save_local").on( "click", function() {
        validateForm();
        save_local();
    });

    $("a#close_player").on( "click", function() {
        destroyLocalPlayer();
        window.location.href = "/";
    });

    function getLocalPlayer() {
        let player = localStorage.getItem('player')
        
        if (player == null) {
            open_modal_player()
            return false
        }
        return true
    }

    function save_local() {
        var name = "";
        var last_name = "";
        var email = "";

        name = $('#name').val();
        last_name = $('#last_name').val();
        email = $('#email').val();

        if (name != "" || last_name!= "" || email != "") {
            let player = {name:name, last_name:last_name, email:email};
            localStorage.setItem('player', JSON.stringify(player));
            window.location.reload();
        }
    }

    function destroyLocalPlayer() {
        // localStorage.removeItem('player');
    }

    function destroyGameData() {
        localStorage.removeItem('game1');
        localStorage.removeItem('game2');
        localStorage.removeItem('game3');
    }

    var display = document.querySelector('#time');
    minutes = 60 * 5;
    startTimer(minutes, display);

    // $("#begin_game").click(function() {
    //     toggle_visibility_container(1, "none");
    //     toggle_visibility_container(2, "block");
    //     var display = document.querySelector('#time');
    //     minutes = 60 * 15;
    //     startTimer(minutes, display);
    // });
    
    function toggle_visibility_container(id, value) {
       var e = document.getElementById("container_"+id);
       e.style.display = value;
    }


    function startTimer(duration, display) {
        var status = false;
        var timer = duration, minutes, seconds;
        var interval = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            display.textContent = minutes + ":" + seconds;
            if (--timer < 0) {
                clearInterval(interval)
                var input_component_selected_1 = $('#input_component_selected_1').val();
                var input_component_selected_2 = $('#input_component_selected_2').val();
                var input_component_selected_3 = $('#input_component_selected_3').val();
                var input_component_selected_4 = $('#input_component_selected_4').val();
                var diagram_option_value = $('#diagram_option_value').val();
                let components = {
                    input_component_selected_1: input_component_selected_1,
                    input_component_selected_2: input_component_selected_2,
                    input_component_selected_3: input_component_selected_3,
                    input_component_selected_4: input_component_selected_4,
                    diagram_option_value: diagram_option_value
                }
                sync_data(components)

                // timer = 0;
                // display.textContent = "Se acabó el tiempo! Gracias por participar. Empieza de nuevo.";
                // display.classList.remove('text-primary');
                // display.classList.add('text-danger');
                // var tr = document.getElementById("title-remaining");
                // //tr.style.display = 'none';
                // setTimeout(function(){
                //     window.location.href =  window.baseUrl
                // }, 2000);
            }
        }, 1000);
    }

    

});